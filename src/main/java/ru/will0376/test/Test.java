package ru.will0376.test;

import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = Test.MOD_ID, name = Test.MOD_NAME, version = Test.VERSION)
public class Test {

	public static final String MOD_ID = "test";
	public static final String MOD_NAME = "Test";
	public static final String VERSION = "1.0";

	@Mod.Instance(MOD_ID)
	public static Test INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		if (FMLCommonHandler.instance().getSide().isClient()) {
			OBJLoader.INSTANCE.addDomain(MOD_ID);
		}
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {

	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {

	}
}
