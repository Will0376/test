package ru.will0376.test;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelTestArmor extends ModelBiped {
	public ModelRenderer helmet;
	//public ModelRender knightChestplate
	//public ModelRender knightLeggings
	//public ModelRender knightBoots

	public ModelTestArmor(){
		this.textureWidth = 256;
		this.textureHeight = 256;
		this.helmet = new ModelRenderer(this, 0, 0);
		this.bipedHead.addChild(helmet);

	}
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5){
		super.render(entity,f,f1,f2,f3,f4,f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

	public void setRotationAngles(ModelRenderer modelRenderer, float x, float y, float z){
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

}
