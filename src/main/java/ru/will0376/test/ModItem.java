package ru.will0376.test;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import sun.management.MethodInfo;

import java.lang.reflect.Method;

public class ModItem {

	public static final ItemArmor.ArmorMaterial ARMOR_MATERIAL = EnumHelper.addArmorMaterial("test:armormaterial", "test:helmet", 9, new int[]{2, 4, 6, 3}, 7, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 2.0F).setRepairItem(new ItemStack(Item.getItemFromBlock(Blocks.OBSIDIAN)));
	public static final TestArmor BOOTS = new TestArmor("boots", ARMOR_MATERIAL, 1, EntityEquipmentSlot.FEET);
	public static final TestArmor LEGGS = new TestArmor("leggs", ARMOR_MATERIAL, 2, EntityEquipmentSlot.LEGS);
	public static final TestArmor CHESTPLATE = new TestArmor("chestplate", ARMOR_MATERIAL, 1, EntityEquipmentSlot.CHEST);
	public static final TestArmor HEAD = new TestArmor("head", ARMOR_MATERIAL, 1, EntityEquipmentSlot.HEAD);
}
