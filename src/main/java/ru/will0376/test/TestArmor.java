package ru.will0376.test;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

public class TestArmor extends ItemArmor {
	public TestArmor(String name, ArmorMaterial materialIn, int renderIndexIn, EntityEquipmentSlot equipmentSlotIn) {
		super(materialIn, renderIndexIn, equipmentSlotIn);
		setTranslationKey(name);
		setRegistryName(name);
		setCreativeTab(CreativeTabs.COMBAT);
	}

	public void registerModels() {
		ModelLoader.setCustomModelResourceLocation(this,0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}

	@Override
	public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack itemStack, EntityEquipmentSlot armorSlot, ModelBiped _default){
//		if(itemStack!=ItemStack.EMPTY){
//			if(itemStack.getItem() instanceof ItemArmor){
				ModelTestArmor model = new ModelTestArmor();
				model.bipedHead.showModel = armorSlot == EntityEquipmentSlot.HEAD;
				model.bipedHeadwear.showModel = false;
				model.isChild = _default.isChild;
				model.isRiding = _default.isRiding;
				model.isSneak = _default.isSneak;
				model.rightArmPose = _default.rightArmPose;
				model.leftArmPose = _default.leftArmPose;
				return model;
//			}
//		}
//		return null;
	}
}
